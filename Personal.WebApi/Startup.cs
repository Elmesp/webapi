﻿namespace Personal.WebApi
{
    using System.Collections.Generic;
    using System.Net;
    using AspNetCoreRateLimit;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using NLog.Extensions.Logging;
    using NLog.Web;
    using Personal.Datasource.SqlServer;
    using Personal.DomainModel.Entities;
    using Personal.DomainModel.Repositories;
    using Personal.WebApi.Models;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.ReturnHttpNotAcceptable = true;
                options.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                options.InputFormatters.Add(new XmlSerializerInputFormatter());
            });

            services.AddSingleton<IClientRepository, ClientRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper, UrlHelper>(factory =>
            {
                var context = factory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(context);
            });

            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(config =>
            {
                config.GeneralRules = new List<RateLimitRule>
                {
                    new RateLimitRule
                    {
                        Endpoint = "*",
                        Limit = 10,
                        Period = "1s"
                    }
                };
            });

            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory logger)
        {
            logger.AddNLog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async handler =>
                    {
                        var exceptionHandler = handler.Features.Get<IExceptionHandlerFeature>();

                        if (exceptionHandler != null)
                        {
                            var factory = logger.CreateLogger("Global API exception logger.");
                            factory.LogError(
                                (int)HttpStatusCode.InternalServerError,
                                exceptionHandler.Error,
                                exceptionHandler.Error.Message);
                        }

                        handler.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await handler.Response.WriteAsync("An error occurred. Try again later.");
                    });
                });
            }

            Mapper.Initialize(config =>
            {
                config.CreateMap<Client, ClientDto>();
                config.CreateMap<Client, ClientForUpdateDto>();
            });

            app.UseIpRateLimiting();
            app.UseMvc();
            app.AddNLogWeb();

            env.ConfigureNLog("nlog.config");
        }
    }
}