﻿namespace Personal.WebApi.Models
{
    public class ClientsPaginationDto : PaginationDto
    {
        public ClientsPaginationDto() : base(10, 20)
        {
            OrderBy = "Name";
        }

        public string Name { get; set; }
    }
}