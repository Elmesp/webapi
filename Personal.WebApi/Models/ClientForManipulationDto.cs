﻿namespace Personal.WebApi.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ClientForManipulationDto
    {
        [Required]
        [MaxLength(256, ErrorMessage = "The name cannot have more than 256 characters.")]
        public string Name { get; set; }
    }
}