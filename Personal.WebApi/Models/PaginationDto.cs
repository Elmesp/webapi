﻿namespace Personal.WebApi.Models
{
    using System;
    using System.Linq;
    using Personal.DomainModel.Values;

    public abstract class PaginationDto
    {
        private int pageSize;

        protected PaginationDto(int pageSize, int maxPageSize)
        {
            this.pageSize = pageSize;
            MaxPageSize = maxPageSize;
            PageNumber = 1;
        }

        public int MaxPageSize { get; }
        public int PageNumber { get; set; }
        public string OrderBy { get; set; }
        public string SortBy { get; set; }

        public SortingOrder SortingOrder
        {
            get
            {
                if (string.IsNullOrWhiteSpace(SortBy))
                {
                    return SortingOrder.Descending;
                }
                return SortBy.Equals("asc", StringComparison.InvariantCultureIgnoreCase)
                    ? SortingOrder.Ascending
                    : SortingOrder.Descending;
            }
        }

        public int PageSize
        {
            get => pageSize;
            set => pageSize = value > MaxPageSize ? MaxPageSize : value;
        }
    }
}