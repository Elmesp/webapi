namespace Personal.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Personal.DomainModel.Entities;
    using Personal.DomainModel.Repositories;
    using Personal.WebApi.Helpers.Types;
    using Personal.WebApi.Models;

    [Route("api/clients")]
    public class ClientsController : RootController
    {
        private readonly IClientRepository clientRepository;
        private readonly IUrlHelper urlHelper;
        private readonly ILogger logger;

        public ClientsController(
            IClientRepository clientRepository,
            IUrlHelper urlHelper,
            ILogger<ClientsController> logger)
        {
            this.clientRepository = clientRepository;
            this.urlHelper = urlHelper;
            this.logger = logger;
        }

        [HttpOptions]
        public IActionResult Options()
        {
            logger.LogInformation("OPTIONS requested.");
            Response.Headers.Add("Allow", "OPTIONS,POST");
            return Ok();
        }

        [HttpHead]
        [HttpGet("{id}", Name = "GetClient")]
        public IActionResult Get(Guid id)
        {
            var client = clientRepository.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            var clientDto = Mapper.Map<ClientDto>(client);

            return Ok(clientDto);
        }

        [HttpHead]
        [HttpGet(Name = "GetClients")]
        public IActionResult Get(ClientsPaginationDto paginationParams)
        {
            var clients = clientRepository.GetClients(
                paginationParams.PageNumber, paginationParams.PageSize,
                paginationParams.Name, paginationParams.OrderBy, paginationParams.SortingOrder);

            var clientsDto = Mapper.Map<IEnumerable<ClientDto>>(clients);

            var prevPageLink = clients.HasPrevious ? CreateClientsUri(paginationParams, ResourceUriType.PreviousPage) : null;
            var nextPageLink = clients.HasNext ? CreateClientsUri(paginationParams, ResourceUriType.NextPage) : null;

            var paginationMetadata = new
            {
                clients.TotalCount,
                clients.PageSize,
                clients.CurrentPage,
                clients.TotalPages,
                prevPageLink,
                nextPageLink
            };

            var paginationHeader = SerializeObject(paginationMetadata);
            Response.Headers.Add("X-Pagination", paginationHeader);

            return Ok(clientsDto);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ClientForCreationDto clientForCreationDto)
        {
            if (clientForCreationDto == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            var client = Client.Factory.Create(clientForCreationDto.Name);
            clientRepository.Create(client);
            var clientDto = Mapper.Map<ClientDto>(client);

            return CreatedAtRoute("GetClient", new { id = client.Id }, clientDto);
        }

        [HttpPost("{id}")]
        public IActionResult BlockPost(Guid id)
        {
            var client = clientRepository.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            return Conflict();
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, [FromBody] JsonPatchDocument<ClientForUpdateDto> patch)
        {
            if (patch == null)
            {
                return BadRequest();
            }

            var client = clientRepository.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            var clientForUpdate = Mapper.Map<ClientForUpdateDto>(client);
            patch.ApplyTo(clientForUpdate, ModelState);

            TryValidateModel(clientForUpdate);

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            client.Update(clientForUpdate.Name);
            clientRepository.Update(client);

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] ClientForUpdateDto clientForUpdateDto)
        {
            if (clientForUpdateDto == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            var client = clientRepository.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            client.Update(clientForUpdateDto.Name);
            clientRepository.Update(client);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var client = clientRepository.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            clientRepository.Delete(id);

            return NoContent();
        }

        private string CreateClientsUri(ClientsPaginationDto paginationParams, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return urlHelper.Link("GetClients", new
                    {
                        paginationParams.Name,
                        pageNumber = paginationParams.PageNumber - 1,
                        paginationParams.PageSize
                    });
                case ResourceUriType.NextPage:
                    return urlHelper.Link("GetClients", new
                    {
                        paginationParams.Name,
                        pageNumber = paginationParams.PageNumber + 1,
                        paginationParams.PageSize
                    });
                default:
                    var e = new ArgumentOutOfRangeException(nameof(type), type, "ResourceUriType not defined.");
                    logger.LogError(e, "Received a ResourceUriType not defined.", type);
                    throw e;
            }
        }
    }
}