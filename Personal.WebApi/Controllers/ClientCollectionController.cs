﻿namespace Personal.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using Personal.DomainModel.Repositories;
    using Personal.WebApi.Helpers.Binders;
    using Personal.WebApi.Models;

    [Route("api/clientcollection")]
    public class ClientCollectionController : RootController
    {
        private readonly IClientRepository clientRepository;

        public ClientCollectionController(IClientRepository clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        [HttpOptions]
        public IActionResult Options()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS");
            return Ok();
        }

        [HttpHead]
        [HttpGet("({ids})")]
        public IActionResult GetClientsCollection([ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }

            var clients = clientRepository.GetByIds(ids);

            if (!clients.Any())
            {
                return NotFound();
            }

            var clientsDto = Mapper.Map<IEnumerable<ClientDto>>(clients);
            return Ok(clientsDto);
        }
    }
}