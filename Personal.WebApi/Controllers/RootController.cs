﻿namespace Personal.WebApi.Controllers
{
    using Library.WebApi.Helpers.HttpResults;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public abstract class RootController : Controller
    {
        [NonAction]
        public ConflictResult Conflict()
        {
            return new ConflictResult();
        }

        [NonAction]
        public UnprocessableEntityResult UnprocessableEntity()
        {
            return new UnprocessableEntityResult();
        }

        [NonAction]
        public UnprocessableEntityObjectResult UnprocessableEntity(ModelStateDictionary modelState)
        {
            return new UnprocessableEntityObjectResult(modelState);
        }

        protected string SerializeObject(object value)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return JsonConvert.SerializeObject(value, settings);
        }
    }
}