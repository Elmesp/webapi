﻿namespace Personal.WebApi.Helpers.Types
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage
    }
}