﻿namespace Library.WebApi.Helpers.DataAnnotations
{
    using System.Collections;
    using System.ComponentModel.DataAnnotations;

    public class NotEmptyCollectionAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var strings = value as ICollection;

            return strings != null && strings.Count > 0;
        }
    }
}