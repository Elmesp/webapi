﻿namespace Library.WebApi.Helpers.HttpResults
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class ConflictResult : StatusCodeResult
    {
        public ConflictResult() : base(StatusCodes.Status409Conflict)
        {
        }
    }
}