﻿namespace Library.WebApi.Helpers.HttpResults
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class UnprocessableEntityResult : StatusCodeResult
    {
        public UnprocessableEntityResult() : base(StatusCodes.Status422UnprocessableEntity)
        {
        }
    }
}