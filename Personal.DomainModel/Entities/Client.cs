﻿namespace Personal.DomainModel.Entities
{
    using System;

    public class Client : Entity<Guid>
    {
        private Client(Guid id, string name) : base(id)
        {
            Name = name;
        }
        
        public string Name { get; private set; }

        private static void Validate(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw  new ArgumentNullException(nameof(name), "The name is null or empty.");
            }

            if (name.Length > 256)
            {
                throw new ArgumentOutOfRangeException(nameof(name), name.Length, "The name cannot have more than 256 characters.");
            }
        }

        public void Update(string name)
        {
            Validate(name);

            Name = name;
        }

        public static class Factory
        {
            public static Client Create(string name)
            {
                Validate(name);
                var id = Guid.NewGuid();

                return new Client(id, name);
            }

            public static Client Read(Guid id, string name)
            {
                Validate(name);

                return new Client(id, name);
            }
        }
    }
}