﻿namespace Personal.DomainModel.Entities
{
    public interface IEntity<out T>
    {
        T Id { get; }
    }
}