﻿namespace Personal.DomainModel.Repositories
{
    using System.Collections.Generic;

    public interface IRepository<out T, in TId>
    {
        T GetById(TId id);
        IEnumerable<T> GetByIds(IEnumerable<TId>ids);
    }
}