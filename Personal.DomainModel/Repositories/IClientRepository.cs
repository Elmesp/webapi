﻿namespace Personal.DomainModel.Repositories
{
    using System;
    using Personal.DomainModel.Entities;
    using Personal.DomainModel.Values;

    public interface IClientRepository : IRepository<Client, Guid>
    {
        void Create(Client client);
        PagedList<Client> GetClients(int pageNumber, int pageSize, string name, string orderBy, SortingOrder sortOrder);
        void Update(Client client);
        void Delete(Guid id);
    }
}