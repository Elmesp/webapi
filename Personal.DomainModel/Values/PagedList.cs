﻿namespace Personal.DomainModel.Values
{
    using System;
    using System.Collections.Generic;

    public class PagedList<T> : List<T>
    {
        public PagedList(IEnumerable<T> items, int totalCount, int pageNumber, int pageSize)
        {
            TotalCount = totalCount;
            PageSize = pageSize;
            CurrentPage = pageNumber;

            AddRange(items);
        }

        public int TotalCount { get; }
        public int PageSize { get; }
        public int CurrentPage { get; }
        public int TotalPages => (int)Math.Ceiling(TotalCount / (double)PageSize);
        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;
    }
}