﻿namespace Personal.DomainModel.Values
{
    public enum SortingOrder
    {
        Descending,
        Ascending
    }
}