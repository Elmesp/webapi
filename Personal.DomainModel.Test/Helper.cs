﻿namespace Personal.DomainModel.Test
{
    using System;
    using System.Linq;

    internal static class Helper
    {
        public static string GetRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return GenerateRandomString(chars, length);

        }

        public static string GetRandomStringOfDigits(int length)
        {
            const string chars = "0123456789";
            return GenerateRandomString(chars, length);

        }

        public static string GetRandomStringOfLetters(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return GenerateRandomString(chars, length);
        }

        private static string GenerateRandomString(string chars, int length)
        {
            return new string(Enumerable.Repeat(chars, length)
                                        .Select(s => s[new Random().Next(s.Length)])
                                        .ToArray());
        }
    }
}