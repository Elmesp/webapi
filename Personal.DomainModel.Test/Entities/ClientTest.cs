﻿namespace Personal.DomainModel.Test.Entities
{
    using System;
    using Personal.DomainModel.Entities;
    using Xunit;

    public class ClientTest
    {
        [Fact]
        public void CreateFailsWithNameNullOrEmpty()
        {
            var name = string.Empty;

            var e = Assert.Throws<ArgumentNullException>(() => Client.Factory.Create(name));
            Assert.Contains("The name is null or empty", e.Message);
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void CreateFailsWithNameLongerThan256()
        {
            var name = Helper.GetRandomString(300);

            var e = Assert.Throws<ArgumentOutOfRangeException>(() => Client.Factory.Create(name));
            Assert.Contains("The name cannot have more than 256 characters", e.Message);
            Assert.Equal("name", e.ParamName);
            Assert.Equal(300, e.ActualValue);
        }

        [Fact]
        public void CreateSuccess()
        {
            var name = Helper.GetRandomString(20);
            var client = Client.Factory.Create(name);

            Assert.NotEqual(Guid.Empty, client.Id);
            Assert.Equal(name, client.Name);
        }

        [Fact]
        public void ReadFailsWithDefaultId()
        {
            var id = default(Guid);
            var name = Helper.GetRandomString(20);

            var e = Assert.Throws<ArgumentException>(() => Client.Factory.Read(id, name));
            Assert.Contains("The ID cannot be the default value", e.Message);
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void ReadFailsWithNameNullOrEmpty()
        {
            var id = Guid.NewGuid();
            var name = string.Empty;

            var e = Assert.Throws<ArgumentNullException>(() => Client.Factory.Read(id, name));
            Assert.Contains("The name is null or empty", e.Message);
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void ReadFailsWithNameLongerThan256()
        {
            var id = Guid.NewGuid();
            var name = Helper.GetRandomString(300);

            var e = Assert.Throws<ArgumentOutOfRangeException>(() => Client.Factory.Read(id, name));
            Assert.Contains("The name cannot have more than 256 characters", e.Message);
            Assert.Equal("name", e.ParamName);
            Assert.Equal(300, e.ActualValue);
        }

        [Fact]
        public void ReadSuccess()
        {
            var id = Guid.NewGuid();
            var name = Helper.GetRandomString(20);

            var client = Client.Factory.Read(id, name);

            Assert.NotEqual(Guid.Empty, client.Id);
            Assert.Equal(id, client.Id);
            Assert.Equal(name, client.Name);
        }

        [Fact]
        public void UpdateFailsWithNameNullOrEmpty()
        {
            var name = Helper.GetRandomString(20);

            var author = Client.Factory.Create(name);

            name = string.Empty;

            var e = Assert.Throws<ArgumentNullException>(() => author.Update(name));
            Assert.Contains("The name is null or empty", e.Message);
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void UpdateFailsWithNameLongerThan256()
        {
            var name = Helper.GetRandomString(20);
            var author = Client.Factory.Create(name);

            name = Helper.GetRandomString(300);

            var e = Assert.Throws<ArgumentNullException>(() => author.Update(name));
            Assert.Contains("The name cannot have more than 256 characters", e.Message);
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void UpdateSuccess()
        {
            var name = Helper.GetRandomString(20);
            var client = Client.Factory.Create(name);

            name = Helper.GetRandomString(20);

            client.Update(name);

            Assert.Equal(name, client.Name);
        }
    }
}