﻿namespace Personal.Datasource.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Personal.DomainModel.Entities;
    using Personal.DomainModel.Repositories;
    using Personal.DomainModel.Values;

    public class ClientRepository : IClientRepository
    {
        private readonly List<Client> clients = new List<Client>
        {
            Client.Factory.Read(new Guid("a490f648-26f4-4c5b-b820-6786b4cce854"), "Hewlett-Packard"),
            Client.Factory.Read(new Guid("948a2fb1-e93c-42ad-9372-1d9636e33c0d"), "Microsoft Corporation"),
            Client.Factory.Read(new Guid("6ccf13e6-673e-4d80-9388-069ed1667216"), "Verizon Communications")
        };

        public void Create(Client client)
        {
            clients.Add(client);
        }

        public Client GetById(Guid id)
        {
            return clients.Find(x => x.Id == id);
        }

        public IEnumerable<Client> GetByIds(IEnumerable<Guid> ids)
        {
            return clients.Where(x => ids.Contains(x.Id));
        }

        public PagedList<Client> GetClients(int pageNumber, int pageSize, string name, string orderBy, SortingOrder sortOrder)
        {
            var items = clients.ToList();

            if (!string.IsNullOrWhiteSpace(name))
            {
                items = clients.Where(x => x.Name.Contains(name)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                if (orderBy.Equals("Name"))
                {
                    items = (sortOrder == SortingOrder.Descending
                        ? items.OrderByDescending(x => x.Name)
                        : items.OrderBy(x => x.Name)).ToList();
                }
            }

            items = items.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PagedList<Client>(items, clients.Count, pageNumber, pageSize);
        }

        public void Update(Client client)
        {
            var modified = clients.Find(x => x.Id == client.Id);
            modified.Update(client.Name);
        }

        public void Delete(Guid id)
        {
            var returned = clients.Find(x => x.Id == id);
            clients.Remove(returned);
        }
    }
}